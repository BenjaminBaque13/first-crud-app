@extends('base')

@section('main')
<div class="row">
    <div class="container-fluid">
        <h1 class="display-4 text-left">Formulaire d'ajout de voiture</h1>
        <div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br />
            @endif
            <div class="col-sm-6">
                <form method="post" action="{{ route('voitures.store') }}">
                    @csrf
                    <div class="form-group">
                        <label for="marque">Marque :</label>
                        <input type="text" class="form-control" name="marque" />
                    </div>

                    <div class="form-group">
                        <label for="modele">Modèle :</label>
                        <input type="text" class="form-control" name="modele" />
                    </div>

                    <div class="form-group">
                        <label for="annee">Année :</label>
                        <input type="number" class="form-control" name="annee" value="2000" />
                    </div>

                    <button type="submit" class="btn btn-success float-right w-50">Validez</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection