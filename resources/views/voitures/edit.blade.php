@extends('base') 
@section('main')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-3">Modifier une voiture</h1>

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <br /> 
        @endif
        <form method="post" action="{{ route('voitures.update', $voiture->id) }}">
            @method('PATCH') 
            @csrf
            <div class="form-group">

                <label for="marque">Marque :</label>
                <input type="text" class="form-control" name="marque" value={{ $voiture->marque }} />
            </div>

            <div class="form-group">
                <label for="modele">Modèle :</label>
                <input type="text" class="form-control" name="modele" value={{ $voiture->modele }} />
            </div>

            <div class="form-group">
                <label for="annee">Année :</label>
                <input type="number" class="form-control" name="annee" value={{ $voiture->annee }} />
            </div>
            
            <button type="submit" class="btn btn-primary">Modifier</button>
        </form>
    </div>
</div>
@endsection