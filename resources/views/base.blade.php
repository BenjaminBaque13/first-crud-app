<!DOCTYPE html>
<html lang="fr">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Laravel CRUD Tutorial</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <header class="container-fluid p-3 mb-2 bg-dark text-white">
            <h1 class="text-center display-4">Application de gestion de voiture</h1>
            <!-- <img src="/voiture.jpg" alt="alt text"> -->
    </header>
    <div class="container">
        @yield('main')
    </div>
</body>

</html>