@extends('base')

@section('main')

<div class="row">
    <div class="col-sm-12">
        <div class='row'>
            <h1 class="display-4">Liste de voitures</h1>
            <div>
                <a style="margin: 18px;" href="{{ route('voitures.create')}}" class="btn btn-success">Nouvelle voiture</a>
            </div>
        </div>
        <table class="table table-striped">
            <thead>
                <tr class="p-3 mb-2 bg-dark text-white">
                    <td>ID</td>
                    <td>Marque</td>
                    <td>Modele</td>
                    <td>Année</td>
                    <td>Actions</td>
                </tr>
            </thead>
            <tbody>
                @foreach($voitures as $voiture)
                <tr>
                    <td>{{$voiture->id}}</td>
                    <td>{{$voiture->marque}}</td>
                    <td>{{$voiture->modele}}</td>
                    <td>{{$voiture->annee}}</td>
                    <td>
                        <a href="{{ route('voitures.edit',$voiture->id)}}" class="btn btn-primary">Modifier</a>
                        <form action="{{ route('voitures.destroy', $voiture->id)}}" method="post">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger" type="submit">Supprimer</button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div>
        </div>
        @endsection

        <div class="col-sm-12">

            @if(session()->get('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
            @endif
        </div>