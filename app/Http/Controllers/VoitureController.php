<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Voiture;

class VoitureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $voitures = Voiture::all();

        return view('index', compact('voitures'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('voitures.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'marque' => 'required',
            'modele' => 'required',
            'annee' => 'required'
        ]);
        $voiture = new Voiture([
            'marque' => $request->get('marque'),
            'modele' => $request->get('modele'),
            'annee' => $request->get('annee')
        ]);
        $voiture->save();
        return redirect('/voitures')->with('success', 'Nouvelle voiture enregistrée !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		$voiture = Voiture::find($id);

		return View::make('voitures.show')
			->with('voiture', $voiture);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $voiture = Voiture::find($id);
        return view('voitures.edit', compact('voiture'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'marque' => 'required',
            'modele' => 'required',
            'annee' => 'required'
        ]);

        $voiture = Voiture::find($id);
        $voiture->marque =  $request->get('marque');
        $voiture->modele = $request->get('modele');
        $voiture->annee = $request->get('annee');
        $voiture->save();

        return redirect('/voitures')->with('success', 'Voiture modifiée !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $voiture = Voiture::find($id);
        $voiture->delete();

        return redirect('/voitures')->with('success', 'Voiture supprimée !');
    }
}